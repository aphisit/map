//
//  SceneDelegate.h
//  MapTest
//
//  Created by Mac on 4/11/2563 BE.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

